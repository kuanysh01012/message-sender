
### Built With

This section  major frameworks/libraries used to in your project. 

* [![Django][Django]][Django-url]
* [![Celery][Celery]][Celery-url]
* [![Redis][Redis]][Redis-url]
* [![Flower][Flower]][Flower-url]
* [![PostgreSQL][PostgreSQL]][PostgreSQL-url]


<!-- GETTING STARTED -->
## Getting Started

This is instructions on setting up project locally.
To get a local copy up and running follow these simple steps.


### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._


1. Clone the repo
   ```sh
   git clone git@gitlab.com:kuanysh01012/message-sender.git
   ```
2. Drop venv and install your personal venv
   ```sh
   python -m venv venv
   ```
3. Install the requirements
   ```sh
   pip install -r requirements.txt
   ```
4. Change the databases parameter in settings.py
   
5. Make migrations
   ```sh
   python manage.py makemigrations
   python manage.py migrate
   ```   
6. Run the server
   ```sh
   python manage.py runserver
   ```
7. Run the redis-server through WSL (Ubuntu)
   ```sh
   sudo apt update
   sudo apt install redis-server
   redis-cli 
   ```
8. Run the celery server
   ```sh
   celery -A messageSender worker -l info
   ```
9. Run the Flower
   ```sh
   celery -A messageSender flower --port=5555
   ```

<!-- USAGE EXAMPLES -->
## Api urls
localhost:8080/api/ - Api root
localhost:8080/api/clients/ - Clients list, POST new client
localhost:8080/api/clients/{id} - Client instance, PUT, DELETE methods
localhost:8080/api/messages/ - Messages list
localhost:8080/api/senders/ - Senders list, POST new Sender
localhost:8080/api/senders/statistics/ - Get statistics by all senders (How many messages sent or still waiting to send)
localhost:8080/api/senders/{id}/sender_analysis - Get statistics by single sender (How many messages sent or still waiting to send)
localhost:5555/ - Celery Web UI to manage tasks
localhost:8080/docs - OpenAPI


Additional points

3. Cделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
4. реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям

