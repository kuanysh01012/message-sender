from django.shortcuts import render, get_object_or_404
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework.decorators import action, api_view
from rest_framework import viewsets, status

from .models import Sender, Client, Message
from .serializers import SenderSerializer, ClientSerializer, MessageSerializer


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def list(self, request):
        queryset = Client.objects.all()
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def update(self, request, pk=None, *args, **kwargs):
        user = request.user
        instance = self.get_object()
        data = {
            "phone_number": request.POST.get('phone_number', None),
            "tag": request.POST.get('tag', None),
            "timezone": request.POST.get('timezone', None),
        }
        serializer = self.serializer_class(instance=instance,
                                           data=data,  # or request.data
                                           context={'author': user},
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        u = request.user
        queryset = Client.objects.filter(id=u.id, pk=pk)
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = ClientSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()

    def create(self, request):
        raise MethodNotAllowed(method='POST')


class SenderViewSet(viewsets.ModelViewSet):
    serializer_class = SenderSerializer
    queryset = Sender.objects.all()

    def list(self, request):
        queryset = Sender.objects.all()
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def update(self, request, pk=None, *args, **kwargs):
        user = request.user
        instance = self.get_object()
        data = {
            "phone_number": request.POST.get('phone_number', None),
            "tag": request.POST.get('tag', None),
            "timezone": request.POST.get('timezone', None),
        }
        serializer = self.serializer_class(instance=instance,
                                           data=data,  # or request.data
                                           context={'author': user},
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        u = request.user
        queryset = Client.objects.filter(id=u.id, pk=pk)
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = ClientSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def sender_analysis(self, request, pk=None):
        """
        Analysis for a specific sender
        """
        queryset_mailing = Sender.objects.all()
        get_object_or_404(queryset_mailing, pk=pk)
        queryset = Message.objects.filter(sender_id=pk).all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def statistics(self, request):
        """
        Statistics for all senders
        """
        total_count = Sender.objects.count()
        mailing = Sender.objects.values('id')
        content = {'Total number of senders': total_count,
                   'The number of messages sent': ''}
        result = {}

        for row in mailing:
            res = {'Total messages': 0, 'Sent': 0, 'No sent': 0}
            mail = Message.objects.filter(sender_id=row['id']).all()
            group_sent = mail.filter(message_status='Sent').count()
            group_no_sent = mail.filter(message_status='No sent').count()
            res['Total messages'] = len(mail)
            res['Sent'] = group_sent
            res['No sent'] = group_no_sent
            result[row['id']] = res

        content['The number of messages sent'] = result
        return Response(content)
