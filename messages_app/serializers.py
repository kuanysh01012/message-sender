from rest_framework import serializers
from .models import Sender, Client, Message


class SenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sender
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
