# Generated by Django 4.1.1 on 2022-09-09 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messages_app', '0002_remove_sender_send_end_remove_sender_send_start_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='operator_code',
            field=models.CharField(editable=False, max_length=3, verbose_name='Operator code'),
        ),
    ]
