from django.core.validators import RegexValidator
from django.utils import timezone
from django.db import models
import pytz


class Sender(models.Model):
    senddate_start = models.DateTimeField(verbose_name='Mailing start', default=None)
    senddate_end = models.DateTimeField(verbose_name='End of mailing', default=None)
    sendtime_start = models.TimeField(verbose_name="Start of sending messages_app", default=None)
    sendtime_end = models.TimeField(verbose_name="End of sending messages_app", default=None)
    message_text = models.TextField(max_length=255, verbose_name="Message text")
    tag = models.CharField(max_length=255, verbose_name='Tags', blank=True)
    operator_code = models.CharField(verbose_name='Operator code',
                                     max_length=3, blank=True)

    def __str__(self):
        return f'Sending messages_app by sender_id {self.id} started from {self.senddate_start}'

    class Meta:
        verbose_name = 'Sender'
        verbose_name_plural = 'Senders'

    @property
    def to_send(self):
        now = timezone.now()
        if self.senddate_start <= now <= self.senddate_end:
            return True
        else:
            return False


class Client(models.Model):
    """
    To validate phone number we need do it with regex:
    Client's phone number starts from 7 and continuing with 10 decimals
    """
    regex = RegexValidator(regex=r'^7\d{10}$')

    common_timezones = tuple(zip(pytz.common_timezones, pytz.common_timezones))  # get most common timezones
    phone_number = models.CharField(validators=[regex], verbose_name='Phone number', max_length=11, unique=True)
    operator_code = models.CharField(verbose_name='Operator code', max_length=3, editable=False)
    tag = models.CharField(verbose_name='Tags', max_length=255, blank=True)
    timezone = models.CharField(verbose_name='Time zone', max_length=64, choices=common_timezones, default='UTC')

    def save(self, *args, **kwargs):
        self.operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'Client {self.id} with number {self.phone_number}'

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


class Message(models.Model):
    sent = "Message sent"
    working = "Task is working"

    status = [
        (sent, "Message sent"),
        (working, "Task is working"),
    ]

    created_time = models.DateTimeField(verbose_name='Created time', auto_now_add=True)
    message_status = models.CharField(verbose_name='Message status', max_length=15, choices=status)
    sender = models.ForeignKey(Sender, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')

    def __str__(self):
        return f'Message {self.id} by {self.sender} for {self.client}'

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
