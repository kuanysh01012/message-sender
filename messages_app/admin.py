from django.contrib import admin

from .models import Sender, Client, Message


admin.site.register(Sender)
admin.site.register(Client)
admin.site.register(Message)
