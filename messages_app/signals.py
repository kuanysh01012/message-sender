from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from .models import Sender, Client, Message
from .tasks import send_message


@receiver(post_save, sender=Sender, dispatch_uid="create_message")
def create_message(sender, instance, created, **kwargs):
    if created:
        sender = Sender.objects.filter(id=instance.id).first()
        clients = Client.objects.filter(Q(operator_code=sender.operator_code) |
                                        Q(tag=sender.tag)).all()

        for client in clients:
            Message.objects.create(
                message_status="Task is working",
                client_id=client.id,
                sender_id=instance.id
            )
            message = Message.objects.filter(sender_id=instance.id, client_id=client.id).first()
            data = {
                'id': message.id,
                "phone": client.phone_number,
                "text": sender.message_text
            }
            client_id = client.id
            sender_id = sender.id

            if instance.to_send:
                send_message.apply_async((data, client_id, sender_id),
                                         expires=sender.senddate_end)
            else:
                send_message.apply_async((data, client_id, sender_id),
                                         eta=sender.senddate_start, expires=sender.senddate_end)


