import requests
import pytz
import datetime
from celery.utils.log import get_task_logger

from .models import Message, Client, Sender
from messageSender.celery import app

logger = get_task_logger(__name__)

URL = 'https://probe.fbrq.cloud/v1/send/'
TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQxNjYyNTcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imt1YW55c2hfa2Fzc3ltYmF5ZXYifQ.Hqnllhv3iJdBePSJoldDBp5NtdDbE-VNbx6zCAlLtz0'


@app.task(bind=True, retry_backoff=True)
def send_message(self, data, client_id, sender_id, url=URL, token=TOKEN):
    mail = Sender.objects.get(pk=sender_id)
    client = Client.objects.get(pk=client_id)
    timezone = pytz.timezone(client.timezone)
    now = datetime.datetime.now(timezone)

    if mail.sendtime_start <= now.time() <= mail.sendtime_end:
        header = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json'}
        try:
            requests.post(url=url + str(data['id']), headers=header, json=data)

        except requests.exceptions.RequestException as exc:
            logger.error(f"With message id: {data['id']} error occurred")
            raise self.retry(exc=exc)

        else:
            logger.info(f"Message id: {data['id']}, Sending status: 'Sent'")
            Message.objects.filter(pk=data['id']).update(message_status='Sent')
    else:
        # get the current time
        time = 24 - (int(now.time().strftime('%H:%M:%S')[:2]) -
                     int(mail.sendtime_start.strftime('%H:%M:%S')[:2]))
        logger.info(f"Message id: {data['id']}, "
                    f"The current time is not for sending the message,"
                    f"worker will restart the task after {60 * 60 * time} seconds")
        return self.retry(countdown=60 * 60 * time)
