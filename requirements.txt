Django==4.1.1
djangorestframework==3.13.1
drf_yasg==1.21.3
pytz==2022.2.1
requests==2.28.1
